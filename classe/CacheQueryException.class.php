<?php
class CacheQueryException extends ExceptionFile{
	const VALOR_NAO_DEFINIDO = 0;
	const DIRETORIO_INVALIDO = 1;
	const ARQUIVO_INVALIDO   = 2;

    public function CacheQueryException($msg,$cod) {
		parent::__construct($msg,$cod);
		parent::gravarLog(Config::$rootDir."logs/logSistema.txt");
    }
}
?>