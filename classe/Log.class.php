<?php

class Log {

	private $arquivo;
	private $msg;
	private $tentativas;
	
    public function Log() {
    	$this->tentativas = 0;
    }
        
    public function __set($propriedade,$valor) {
		 if($propriedade=='arquivo'){
		 	$this->arquivo=$valor;
		 }else if($propriedade=='tentativas'){
		 	$this->tentativas=$valor;
		 }else if($propriedade=='msg'){
		 	$this->msg=$valor;
		 }
    }

    public function __get($propriedade) {
		 if($propriedade=='arquivo'){
		 	return $this->arquivo;
		 }else if($propriedade=='tentativas'){
		 	return $this->tentativas;
		 }else if($propriedade=='msg'){
		 	return $this->msg;
		 }
    }
        
    public function gravar(){
    	if(is_writable($this->arquivo)){
	    	$link = fopen($this->arquivo,'a');
	    	fwrite($link,$this->msg);
			fclose($link);    	
			$this->tentativas=0;
			return true;	
    	}else if(file_exists($this->arquivo)){
    		if($this->tentativas>2){
    			return false;
    		}else{
    			$this->tentativas++; 
    			return $this->gravar();    		    			
    		}
    	}else{
	    	$link = fopen($this->arquivo,'a');
			fclose($link);    		    		
    		return $this->gravar();
    	}
    }
}
?>