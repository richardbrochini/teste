<?php

class CacheQuery {
	
	private $qry;
	private $result;
	private $dir;
	private $arquivo;
	
    public function CacheQuery() {
    	$this->dir = Config::$rootDir."cacheQuery/";
    }

	public function __set($propriedade,$valor){
		if(strlen($valor)>0){
			if($propriedade=="qry"){
				$this->qry=$valor;
			}else if($propriedade=="result"){
				$this->result=$valor;
			}
		}else{
			throw new CacheQueryException('Valor invalido:',CacheQueryException::VALOR_NAO_DEFINIDO);
		}
	}

	public function __get($propriedade){
		if($propriedade=="qry"){
			return $this->qry;
		}else if($propriedade=="result"){
			return $this->result;
		}		
	}
	
	public function cacheado(){
		$this->arquivo = sha1($this->qry);	
		if(file_exists($this->dir.$this->arquivo)){
			$dia = date("dmY");
			if($dia==date("dmY",fileatime($this->dir.$this->arquivo))){
				$hora = date("His",fileatime($this->dir.$this->arquivo));
				$horaAtual = date("His");
				if(($horaAtual-$hora)<10){
					return true;
				}else{
					return false;
				} 
			}else{
				return false;
			}
		}else{
			return false;
		} 
	}
	
	public function cache(){
		if(file_exists($this->dir.$this->arquivo)){
	    	$link = fopen($this->dir.$this->arquivo,'r');
			$this->result = fread($link,filesize($this->dir.$this->arquivo));
			fclose($link);			
			return $this->result;
		}else{
			throw new CacheQueryException('arquivo invalido:'.$this->dir.$this->arquivo,CacheQueryException::ARQUIVO_INVALIDO);
			return null;
		} 
	}
	
	public function gravar(){
    	if(is_writable($this->dir)){
	    	$link = fopen($this->dir.$this->arquivo,'a');
	    	fwrite($link,$this->result);
			fclose($link);
			return true;    	
   		}else{
			throw new CacheQueryException('diretorio invalido:'.$this->dir,CacheQueryException::DIRETORIO_INVALIDO);
   			return false;
   		}		
	}
}
?>