<?php /* Smarty version 2.6.21, created on 2014-03-12 19:01:08
         compiled from cadastro.tpl */ ?>
<section id="corpo"> <?php echo $this->_tpl_vars['cabecalho']; ?>

<div class="conteudo">
  <div class="esquerda">
    <div class="textoExplicativo">Breve texto explicando
      como funciona o jogo
      e o que tem de legal! </div>
  </div>
  <div class="esquerda">
    <div class="bgQuestoes">
    <div class="texto">Insira os dados pra começar!</div>
    <form action='?acao=cadastrar' method="POST" name="formCadastro" id="formCadastro">
      <div class="form-group">
        <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome"/>
      </div>
      <div class="form-group">
        <input type="text" name="email" class="form-control" id="email" placeholder="E-mail"/>
      </div>
      <div class="form-group">
        <input type="text" name="nascimento" class="form-control" id="nascimento" placeholder="Data de Nascimento"/>
      </div>
      <div class="form-group text-center">
        <label class="checkbox-inline">
          <input type="radio" name="sexo" value="1" data-title="homem" <?php if ($this->_tpl_vars['sexo'] == 1): ?>checked="checked"<?php endif; ?> />
          homem </label>
        <label class="checkbox-inline">
          <input type="radio" name="sexo" value="0" data-title="mulher" <?php if ($this->_tpl_vars['sexo'] == 0): ?>checked="checked"<?php endif; ?>/>
          mulher </label>
      </div>
    </form>
     
  </div>
  <br clear="all">
  	<a href='javaScript:void(0);' onClick="javaScript:document.getElementById('formCadastro').submit();">
		<div class="btn_jogar">Jogar</div>
    </a>

  <br clear="all">
  </div>
  </div>
  <?php echo $this->_tpl_vars['rodape']; ?>

  </div>
</section>