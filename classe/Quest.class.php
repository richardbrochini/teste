<?class Quest{
	 private $idquest;
	 private $title;
	 private $bd;
	 public function Quest() {
		 $this->bd =BD::getInstance();
	}
	 public function __set($propriedade,$valor) {
		 if(strlen($valor)>0){
			 if($propriedade=='idquest'){
				 $this->idquest=$valor;
			 }
			 if($propriedade=='title'){
				 $this->title=$valor;
			 }
		 }else{
		 }
	}
	 public function __get($propriedade) {
		 if($propriedade=='idquest'){
			 return $this->idquest;
		 }
		 if($propriedade=='title'){
			 return $this->title;
		 }
	}
	 public function insert() {
		 $sql ='insert into quest set '.
		 'title=\''.mysql_escape_string($this->title).'\'';
		 $this->bd->query($sql);
		 $this->idquest=$this->bd->getLastId();
	}
	 public function update() {
		 $sql ='update quest set '.
		 'title=\''.mysql_escape_string($this->title).'\''.
		 ' where idquest='.mysql_escape_string($this->idquest).'';
		 $this->bd->query($sql);
	}
	 public function delete() {
		 $sql ='delete from quest '.
		 'where idquest='.mysql_escape_string($this->idquest);
		 $this->bd->query($sql);
	}
	 public function fecht() {
		 $sql ='select * from quest where  '.
		 'idquest='.mysql_escape_string($this->idquest).'';
		 $vetor = $this->bd->resultSet($sql);
		$this->idquest=$vetor[0]['idquest'];
		$this->title=$vetor[0]['title'];
	}
	 public function vetor() {
		 $sql ='select * from quest order by idquest ASC';
		 $vetor = $this->bd->resultSet($sql);
		 $an = new Answers();
		 $x=0;
		 while($vetor[$x]){
			$an->idquest = $vetor[$x]['idquest'];
			$vetor[$x]['answers'] = $an->vetor(); 
			$x++;
		 }
		 return $vetor;
	}
}?>
