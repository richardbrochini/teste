<?php

class BD {

      private $banco;
      private $senha;
      private $usuario;
      private $servidor;
	  private $result;	
	  private $id;
	  private $link;
	  private $cacheQuey;
	  private static $instancia;
	  
     private function BD(){
     	$this->cacheQuey = new CacheQuery();
     	$this->cache     = 1;
     	$this->servidor  = Config::$sqlHost;
     	$this->usuario   = Config::$sqlUser;
     	$this->senha     = Config::$sqlPass;    
     	$this->banco     = Config::$sqlScrema; 	     	
     }
	
	 public static function getInstance(){
	 	if(empty(self::$instancia)){
	 		self::$instancia = new BD();
			return self::$instancia;
	 	}else{
	 		return self::$instancia;
	 	}	
	 }
	 
	 public function __set($propriedade,$valor) {
	 	 if(strlen($valor)>0){
			 if($propriedade=='banco'){
			 	$this->banco=$valor;
			 }else if($propriedade=='senha'){
			 	$this->senha=$valor;
			 }else if($propriedade=='usuario'){
			 	$this->usuario=$valor;
			 }else if($propriedade=='servidor'){
			 	$this->servidor=$valor;
			 }else if($propriedade=='id'){
			 	if(is_int($valor)){
			 		$this->id=$valor;	
			 	}else{
			 		throw new ExceptionDB('Valor invalido, integer era esperado:',ExceptionDB::VALOR_NAO_DEFINIDO);
			 	}
			 }else if($propriedade=='link'){
			 	$this->link=$valor;
			 }else if($propriedade=='result'){
			 	$this->result=$valor;
			 }
	 	 }else{
	 		throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 }		 
	 }
	 
	 public function __get($propriedade) {
		 if($propriedade=='banco'){
		 	if(strlen($this->banco)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}	
		 	return $this->banco;
		 }else if($propriedade=='senha'){
		 	if(strlen($this->senha)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->senha;
		 }else if($propriedade=='usuario'){
		 	if(strlen($this->usuario)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->usuario;
		 }else if($propriedade=='servidor'){
		 	if(strlen($this->servidor)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->servidor;
		 }else if($propriedade=='id'){
		 	if(strlen($this->id)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->id;
		 }else if($propriedade=='link'){
		 	if(strlen($this->link)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->link;
		 }else if($propriedade=='result'){
		 	if(strlen($this->result)<0){
	 			throw new ExceptionDB('Valor invalido:',ExceptionDB::VALOR_NAO_DEFINIDO);		 		
	 	 	}
		 	return $this->result;
		 }
	}

	public function getLastId(){
		return $this->id;	
	}	
		
	public function conectaBanco(){
		if(empty($this->link)){
			$this->link = mysql_connect($this->servidor,$this->usuario,$this->senha);
			if(mysql_select_db($this->banco,$this->link)){
				return true;
			}else{
				throw new ExceptionDB('banco de dados n�o existe',ExceptionDB::ERRO_CONEXAO);
				return false;
			}			
		}else{
			if(mysql_select_db($this->banco,$this->link)){
				return true;
			}else{
				throw new ExceptionDB('n�o � possivel se conectar no banco',ExceptionDB::ERRO_CONEXAO);			
				return false;				
			}	
		}
	}

	public function query($qry){
		if($this->conectaBanco()){
			if($this->result=mysql_query($qry)){	
				$id = mysql_insert_id($this->link);
				if(strlen($id)>0){
					$this->id = $id;	
				}
				return true;	  
			}else{
				throw new ExceptionDB('Sintaxe da query errada:'.$qry,ExceptionDB::ERRO_QUERY);
				return false;
			}	
		}else{
			return false;			
		}		
	}	
	
	public function resultSet($qry){  
	  	$i=0;
	  	$vetor = null;
	  	$this->cacheQuey->qry = $qry;
	  	if($this->cacheQuey->cacheado()){
	  		$this->result = $this->cacheQuey->cache();
	        if($this->result){
		        while($vetor[$i] = mysql_fetch_array($this->result,MYSQL_ASSOC)){
		        	$i++;
		        }
		        return $vetor;	  			  			        	
	        }else{
	        	return null;
	        }
	  	}else if($this->query($qry)){
	        //$this->cacheQuey->result = $this->result;
	        //$this->cacheQuey->gravar();
	        while($vetor[$i] = mysql_fetch_array($this->result,MYSQL_ASSOC)){
	        	$i++;
	        } 
	        return $vetor;	  		
	  	}else{
	  		throw new ExceptionDB('Query n�o retonou um resultSet valido:'.$qry,ExceptionDB::ERRO_QUERY);
	  		return $vetor;
	  	}
	}
	
	public function XML($qry) {
		 if($this->conectaBanco()){
		     if ( strlen($qry) > 10 ) {
		          if ( $result=mysql_db_query($this->banco,$qry) ) {
			  		$return_string = "<?xml version='1.0' encoding='ISO-8859-1'?>\n";
					$num_fields = mysql_num_fields($result);
					$num_rows = mysql_num_rows($result);
					$return_string .= "<corpo>\n";			
					$num=@mysql_num_rows($result);
					if($num==false){
		    			return "<?xml version='1.0' encoding='ISO-8859-1'?>\n<corpo>\n</corpo>\n";
					}
					$num_fiel = @mysql_num_fields($result);
					if($num_fiel==false){
		    			return "<?xml version='1.0' encoding='ISO-8859-1'?>\n<corpo>\n</corpo>\n";
					}
					for($i=0;$i<$num;$i++){
						$return_string .= "\t<conteudo>\n";
								for($a=0;$a<$num_fiel;$a++){
									$curr_key = mysql_field_name($result,$a);							
									$curr_value = htmlspecialchars(mysql_result($result,$i,$curr_key),ENT_QUOTES);					
									$curr_type = mysql_field_type($result,$a);
									$curr_length = mysql_field_len($result,$a);
									$curr_flag = mysql_field_flags($result,$a);
									$return_string .= "\t\t<" . $curr_key . " type=\"" . $curr_type . "\" length=\"" . $curr_length . "\" flags=\"" . $curr_flag . "\">";
									$return_string .= utf8_decode($curr_value);
									$return_string .= "</" . $curr_key . ">\n";
								}
						$return_string .= "\t</conteudo>\n";
						}
					$return_string .= "</corpo>\n";
					return $return_string;  	
		          } else {
		          	throw new ExceptionDB('Sintaxe da query errada:'.$qry,ExceptionDB::ERRO_QUERY);
		    		return "<? xml version='1.0' encoding='ISO-8859-1' ?>\n<corpo>\n</corpo>\n";
		          }
		     }
		 }
		 return "<?xml version='1.0' encoding='ISO-8859-1'?>\n<corpo>\n</corpo>\n";		 	
  }		
}
?>