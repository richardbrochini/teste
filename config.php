<?php
	ini_set("include_path",'.;/var/www/testeP/smarty'); 	
	ini_set("allow_url_fopen", 1);
	ini_set("allow_url_include", 1);

	$smarty_class = '/var/www/testeP/smarty/Smarty.class.php';
	require_once($smarty_class);

	$smarty = new Smarty();
	$smarty->template_dir   = '/var/www/testeP/classe/smarty/templates';
	$smarty->config_dir     = '/var/www/testeP/classe/smarty/config';
	$smarty->cache_dir      = '/var/www/testeP/classe/smarty/cache';
	$smarty->compile_dir    = '/var/www/testeP/classe/smarty/templates_c';
	$smarty->caching        = false;
	$smarty->clear_all_cache();
 
	function __autoload($class_name) {
	    require_once ('classe/'.$class_name.'.class.php');
	}
?>
